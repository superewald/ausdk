Game agnostic UE3 SDK library providing c++ bindings for UE core.

# features

- `Object Definitions`: provide and change structure of unreal objects at runtime using yaml
- `Game Agnostic`: easy integration to almost every UE3 game using global objects and names array
- `Serializible`: export object definitions to yaml

# cpm

Use [CPM]() to include this library into your cmakefile.

```cmake
include(cmake/CPM)
CPMAddPackage("gl:superewald/ausdk@0.0.1")
...
target_link_libraries(${PROJECT_NAME} PRIVATE AUSDK::AUSDK)
```

# build

This library doesn't have a default build target because it is an [object library]() (similiar to a header-only library). 

# test

Run unit tests for this library using [Catch2]().

```bash
$ cmake -B build/ -DAUSDK_BUILD_TESTS=ON
$ cmake --build build/
$ ./build/test/AUSDKTests
```