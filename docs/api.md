The API provides access to the fundamental types of the unreal engine
at runtime and allows dynamic parsing of every object.

## object definitions

An object definition tells the sdk how the memory of the type is aligned at runtime.

The definitions must reside in a folder with the following structure:

- `engine.yml`: basic information about the game/engine
- `<pkg-name>/`:
  - `<object-name>.yml`

An object definition requires the following yaml structure (using `Core.Object` as example):

```yaml
# name of the object 
#  (the namespace will be Package.Name; in this case Core.Object)
name: Object
# properties of the object
properties:
    # a property must define a type, name and offset
  - type: FName
    name: Name
    offset: 0x0
  - type: int
    name: ObjectID
    offset: 0x10
  - type: UClass*
    name: Class
    offset: 0x14
  - type: UObject*
    name: Outer
    offset: 0x18
```

The properties type can be any reference or struct of the core types listed below.

## core types

### `TArray` (*ausdk/core/array.h*)

A type independent (templated) array.

- `void* Data`: pointer to the array data
- `int Count`: current size of the array
- `int Max`: capacity of the array

<details><summary><b>Usage</b></summary>

```cpp
// initialize array
TArray<int> arr = { 50, 20, 10 };
// access element by index
std::cout << arr[0]; // 50
// get current size
std::cout << arr.Count; // 3
// get capacity
std::cout << arr.Max; // 3

// iterate through array
for(auto entry : arr)
    std::cout << entry << " ,"; // 50, 20, 10,

TArray<int> arr2 = { 100, 89, 2 };

// compare array
std::cout << arr == arr2; // false
// merge array
Tarray<int> merged = arr + arr2;
std::cout << merged.Count; // 6
```

</details>

<details><summary><b>Definition (<i>core/array.yml</i>)</b></summary>

```yaml
name: Array
map: TArray
properties:
  - name: Data
    type: void*
    offset: 0x0
  - name: Count
    type: int
    offset: 0x4
  - name: Max
    type: int
    offset: 0x8
```

</details>

### `TMap` (*ausdk/core/map.h*)

### `FString` (*ausdk/core/string.h*)

Struct containing a string (TArray of chars).

<details><summary><b>Usage</b></summary>

```cpp
FString str = "Hello World";

// parse to char*
char* chr = str;
// parse to std::string
std::string = str;

// since FString is a TArray, you can also iterate over it
for(char& c : str)
    std::cout << c;
// and access by index
char e = str[1];
```
</details>

<details><summary><b>Defintion</b></summary>

`FString` is just a wrapper for `TArray<char>` and thus doesnt require a definition.
</details>

### `FName` (*ausdk/core/name.h*)



### `UObject` (*ausdk/core/object.h*)

The base type and class for all objects.

*Properties*
- `static TArray<UObject*> Global`: array containing all UObjects 
- `FName Name`: object name (unique at package scope)
- `int ObjectID`: global unique ID of object (index of object in global array)
- `UClass* Class`: the class type of the object
- `UObject* Outer`: object which contains this object

*Methods*
- `bool IsA(std::string className)`
- `bool IsOf(std::string className)`
- 
<details><summary><b>Usage</b></summary>

```cpp
UObject* byID = UObject::Global[200];
UObject* byName = UObject::Global["Class Core.Function"];

auto obj = byName;
cout << obj->Name << obj->ObjectID << ...;
```
</details>

<details><summary><b>Definition</b></summary>

```yaml
name: Object
map: UObject
properties:
  - name: Name
    type: FName
    offset: 0x0
  - name: ObjectID
    type: int
    offset: 0x10
  - name: Class
    type: UClass*
    offset: 0x14
  - name: Outer
    type: UObject*
    offset: 0x18
```
</details>

### `UStruct` (*ausdk/core/struct.h*)

### `UClass` (*ausdk/core/class.h*)
## dynamic types

