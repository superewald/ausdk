# UE3 Concepts

To get a better understanding of the goals of this project and how
they are achieved, you first have to understand the basic concepts behind
UE3 and UnrealScript in particular.

UE3 has a reflection system integrated which is used
to bind Unreal Script into the native c++ core. Most of the code and assets are
represented in that reflection system, only exceptions are material graphs, the core package and
native code (which sometimes is wrapped with Unreal Script which at least reveals
the interfaces).

All these objects are associated with packages (which are equal to the packages the original game developers defined)
and provide information about the object itself.

The `Core` package is the only exception to the reflection system as
it is pure wrapper for native c++. While the exact memory structure differs
from game to game the crucial information stays persistent and offers a way to
reconstruct every other package completely.

All other packages rely at least on the `Core` package and implement
`UClass` to provide any kind of game logic.

Let's dive into the `Core` package first.

## `Core`

The `Core` package is the foundation of the whole engine. It provides
bindings into the native c++ core and runtime reflection for UnrealScript features.

The exact content of this package can differ for different versions of the same game as for different versions aswell.
Still, there are some classes which remain almost the same or at least provide
a persistent interface. These interfaces can be used to reverse the structure of derived
objects (from other packages) at runtime.

The crucial objects which allows access to this reflection are described below.


### `Core.Object` *(`UObject`)*
Furthermore, almost every ingame object (including UI) derives from one single class: `Object` (often called `UObject`).
The `Object` class provides at least the following information about the given object:

| Property | Type | Description |
|---|----------|-------------|
| Name | `FName` | A struct with name reference to the object name (**not unique**) |
| ObjectID | `int` | A unique identifier for the object (which is also the index of the object in the global array) |
| Class | `UClass*` | Reference to `UClass` which represents an UnrealScript class |
| Outer | `UObject*` | Reference to object containing this object (used for nesting in packages eg) |

---
### `Core.Field` *(`UField`)*

`UField`s are simply linked lists implementing only 1 property:
- `UField* Next`: Reference to next field in linked list

---
### `Core.Struct` *(`UStruct`)*
`UStruct`s define hierarchical structured data such as `UClass`es.
A `UStruct` contains the following information:


| Property | Type | Description                                       |
|---|----------|---------------------------------------------------|
| Base | `UField*` | Reference to base struct/class                    |
| Properties | `UField*` | Reference to first property                       |
| PropertySize | `int` | Size of properties (= sizeof native struct/class) |

---
### `UClass`

The most interesting object is the `UClass`. Every object in the game is associated with a class through
its `Class` field.

`UClass` derives (implicitly) from `UStruct` and adds the following assertions to the properties:

- `UStruct::Super` is of type `UClass*` and represents base class
- All properties of `UStruct::Properties` derive from `UProperty`.

The properties of `UClass` are currently unknown to me but that doesn't even matter
as the `UStruct` provides all information about the class itself.

# UnrealScript to C++ mapping

Consider the following definition of a class in UnrealScript:

```
class Simpsons;

var string name = "Simpson";
var string smartness = "?";
var int weight = 0;
```
```
class Homer extends Simpsons;

var string prename = "Homer";
var int weight = 200;
var string smartness = "donut";

function DonatOfDoom() {}
```

Which gets translated to equivalent c++ classes from unreal compiler:

```cpp
class USimpson : Core::UClass
{
public:
    FString name;
    FString smartness;
    int weight;
    
    USimpson(FString name, FString smartness = "?", int weight = 0)
        : name(name), smartness(smartness), weight(weight) {}
};

class UHomer : USimpsons
{
public:
    void DonatOfDoom();
    
    UHomer() : USimpson("Homer", "donuts!!!!", 200) {}
};
```

In this case, the `UClass` properties will look like this:

- `USimpson::Base`: null
- `USimpson::Children`:
  - `UStringProperty( name )::Next`:
    - `UStringProperty( smartness )::Next`:
      - `UIntProperty( weight )`
- `USimpson::PropertySize`: 68

and

- `UHomer::Base`: `USimpson`
- `UHomer::Children`:
  - `UFunctionProperty( DonatOfDoom )`
- `USimpson::PropertySize`: 4

We can abuse this knowledge in order to traverse through all the classes
in the game and parse information about its structure.

## access properties at runtime

Assuming we compiled the UnrealScript above inside a package called `MovieCharacters`,
we can reconstruct the classes at runtime using the sdk as follows:

```cpp
#include <iostream>
#include <ausdk/core/class.h>

using namespace ausdk;
using namespace std;

void PrintClassProperties(UClass* cls)
{
    // print class namespace (Package.ClassName)
    cout << "Class " << cls->PackageName << "." << cls->Name << ":" << endl;
    
    // print properties (linked with UField::Next, starting with UStruct::Children)
    for(auto prop = cls->Children; prop; prop = prop->Next)
        // the class of the property represents its type
        cout << "\tProp (" << cls->Class->Name << ") " << cls->Name << endl;
}

int main()
{
    // get pointer to USimpson/UHomer class and print properties
    PrintClassProperties(UClass::Get("MovieCharacters.Simpson"));
    PrintClassProperties(UClass::Get("MovieCharacters.Homer"));
}
```
> In real life you would have to define the structure of the `Core` objects
> using the [`yaml definitions`]() to make the above snippet work.
> 
Injecting this code into the game at runtime will output the following:
```
Class MovieCharacters.Simpson:
  Prop (StringProperty) name
  Prop (StringProperty) smartness
  Prop (IntProperty)    weight
Class MovieCharacters.Homer:
  Prop (FunctionProperty) DonatOfDoom
```

This is a very simplified example of course but it shows how the reflection
system can be used to reconstruct the game data from binary.

At this point you should be able to understand the basics and benefits of the [API]().