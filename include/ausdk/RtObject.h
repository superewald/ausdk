#pragma once

#include <yaml-cpp/yaml.h>
#include <vector>
#include <string>

namespace ausdk
{
    struct RtProperty
    {
        std::string Name;
        std::string Type;
        int Offset;
    };

    class RtStructure
    {
    public:
        RtStructure(YAML::Node yml)
            : name(yml["name"].as<std::string>()), package(yml["package"].as<std::string>())
        {
            
        }

        std::string& Name();
        std::string& Package();
        std::string& Namespace();

        std::vector<RtProperty*>& Properties();
    private:
        std::string name;
        std::string package;
        std::vector<RtProperty*> props;
    };

    /**
     * @brief 
     *  A class which represents the memory structure of any object at runtime 
     *  without having to define the property at compilation.
     */
    class RtObject
    {
    public:
        /**
         * @brief Construct a new Rt Object object
         * 
         * @param template  template of the object
         * @param object    pointer to actual object in memory
         */
        RtObject(RtStructure* tpl, void* object);

        /**
         * @brief Get property by name
         * 
         * @param propertyName name of property
         * @return RtProperty  searched property
         */
        RtProperty operator [](std::string propertyName);
    };
}
